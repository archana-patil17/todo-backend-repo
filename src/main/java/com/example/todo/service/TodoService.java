package com.example.todo.service;

import com.example.todo.dto.TaskDto;
import com.example.todo.entity.Task;
import com.example.todo.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TodoService {

    TodoRepository repository;

    @Autowired
    public TodoService(TodoRepository repository) {
        this.repository = repository;
    }

    public List<TaskDto> getTasks() {

        List<TaskDto> taskList = new ArrayList<>();

        for (Task task : repository.findAll()) {
            taskList.add(TaskDto.dtoFrom(task));
        }

        return taskList;
    }

    public TaskDto save(TaskDto taskDto) {

        Task task = Task.entityFrom(taskDto);
        Task savedTaskEntity = repository.save(task);
        return TaskDto.dtoFrom(savedTaskEntity);

    }

    public TaskDto updateTask(Integer id, TaskDto taskDto) {

        Optional<Task> task = repository.findById(id);

        Task taskToUpdate = new Task();
        //Update Parameters if record presents or else create New record
        if(task.isPresent())
        {
            taskToUpdate = task.get();
        }

        taskToUpdate.setTask(taskDto.getTask());
        taskToUpdate.setStatus(taskDto.getStatus());

        Task savedTaskEntity = repository.save(taskToUpdate);


        return taskDto.dtoFrom(savedTaskEntity);

    }

    public void deleteById(int id) {
        repository.deleteById(id);
    }

    public Optional<TaskDto> getTaskById(Integer id) {

        Optional<Task> task = repository.findById(id);
        return task.map(TaskDto::dtoFrom);
    }
}
