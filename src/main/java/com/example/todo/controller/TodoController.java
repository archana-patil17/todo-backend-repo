package com.example.todo.controller;

import com.example.todo.dto.TaskDto;
import com.example.todo.entity.Task;
import com.example.todo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:3001")
@RequestMapping("/api/v1/todo")
public class TodoController {

    private TodoService todoService;

    // Constructor based AutoWiring
    @Autowired
    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @PostMapping("/new")
    public ResponseEntity createTask(@RequestBody TaskDto taskDto) throws URISyntaxException {
        TaskDto savedTask = todoService.save(taskDto);
        return  ResponseEntity.ok().body(savedTask);
//        return ResponseEntity.created(new URI("/todo/" + savedTask.getId())).body(savedTask);
    }

    @GetMapping("/")
    public ResponseEntity<List<TaskDto>> getTasks(){
        List<TaskDto> result = todoService.getTasks();
        return  ResponseEntity.ok().body(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskDto> getTaskById(@PathVariable Integer id) {
        Optional<TaskDto> task = todoService.getTaskById(id);
        if (task.isPresent()) {
            return ResponseEntity.ok().body(task.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<TaskDto> updateTask(@PathVariable Integer id, @RequestBody TaskDto taskDto) {

        TaskDto result = todoService.updateTask(id,taskDto);
        return ResponseEntity.accepted().body(result);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteTask(@PathVariable int id) {
        todoService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
