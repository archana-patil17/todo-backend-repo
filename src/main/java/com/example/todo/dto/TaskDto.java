package com.example.todo.dto;

import com.example.todo.entity.Task;

import java.util.Objects;

public class TaskDto {

    Integer id;
    String task;
    String status;

    public TaskDto() {
    }

    public TaskDto(Integer id, String task, String status) {
        this.id = id;
        this.task = task;
        this.status = status;
    }

    public static TaskDto dtoFrom(Task task) {
        return new TaskDto(task.getId(), task.getTask(), task.getStatus());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskDto taskDto = (TaskDto) o;
        return Objects.equals(id, taskDto.id) && Objects.equals(task, taskDto.task) && Objects.equals(status, taskDto.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, task, status);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
